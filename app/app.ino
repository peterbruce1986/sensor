#include "Adafruit_SGP30.h"
#include "Adafruit_BME680.h"
//#include <I2S.h>
// #include <SPI.h>
#include "wiring_private.h" // pinPeripheral() function
#include <Wire.h>

//SPIClass spi (&sercom2, 3, 5, 4, SPI_PAD_0_SCK_3, SERCOM_RX_PAD_1);

TwoWire myWire(&sercom4, 15, 16);


#define DEVICE_SEN
#define DEVICE_BME


#define SGP 0x58
#define GKD 0x56
#define IMP 0x49
#define MOT 0x55
#define SEN 0x5E
#define BME 0x76

#define SOF  0x00FF
#define AEROBIC_HEART_RATE_LOWER_LIMIT  0x2A7E
#define AEROBIC_HEART_RATE_UPPER_LIMIT  0x2A84
#define AEROBIC_THRESHOLD               0x2A7F
#define CMD_TVOC                        0x0001
#define CMD_ECO2                        0x0002
#define CMD_TEMPERATURE                 0x0003
#define CMD_PRESSURE                    0x0004
#define CMD_HUMIDITY                    0x0005
#define CMD_GAS                         0x0006
#define CMD_ALTITUDE                    0x0007
#define EOF                             0xFF00

uint16_t length = 0;

typedef struct
{
  union
  {
    uint8_t _8bit[32];
    struct
    {
      uint16_t _16bit[16];
    }__attribute__((packed));
    struct
    {
      uint32_t _32bit[8];
    }__attribute__((packed));
  };
}t_packet;

t_packet _pkt;

/*
BME280/680-SPI
-temperature
-humidity
-pressure
-gas
SGP30-I2C
-gas
ADMP401-ADC
-noise
SPH0645LM4H-I2S
-noise
SHARP GP2Y0A41SK0F-UART
-distance
*/

/* SGP
#define CMD_GET_SERIAL_ID 0x3682 *
#define CMD_INIT_AIR_QUAL 0x2003
#define CMD_MEAS_AIR_QUAL 0x2008 *
#define CMD_GET_BASELINE  0x2015
#define CMD_SET_BASELINE  0x201E
#define CMD_SET_HUMIDITY  0x2061
#define CMD_MEAS_TEST     0x2032
#define CMD_FEAT_SET_VER  0x202F
#define CMD_MEAS_RAW_SIG  0x2050
*/

/* IMP
IMP_I2CCMD_GET_ID,
IMP_I2CCMD_GET_ADC,
IMP_I2CCMD_NEO_PIN,
IMP_I2CCMD_NEO_SPEED,
IMP_I2CCMD_NEO_BUF_LEN,
IMP_I2CCMD_NEO_CLR,
IMP_I2CCMD_NEO_SHOW,
IMP_I2CCMD_NEO_SET,

IMP_I2CCMD_LAST, 
*/

byte rxBuf[4];
byte txBuf[16];

#ifdef DEVICE_SGP
Adafruit_SGP30 sgp;
#endif

#ifdef DEVICE_BME
#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BME680 bme(&myWire);
#endif

void setup()
{
  delay(2000);
  Serial.begin(9600);

#ifdef DEVICE_SGP
  Wire.begin(SGP); 

  if (! sgp.begin(&myWire)){
    Serial.println("Sensor not found :(");
    while (1);
  }
  Serial.print("Found SGP30 serial #");
  Serial.print(sgp.serialnumber[0], HEX);
  Serial.print(sgp.serialnumber[1], HEX);
  Serial.println(sgp.serialnumber[2], HEX);
#endif

#ifdef DEVICE_BME
  Wire.begin(BME); 

  if (!bme.begin(0x76)) {
    Serial.println("Could not find a valid BME680 sensor, check wiring!");
    while (1);
  }

  // Set up oversampling and filter initialization
  bme.setTemperatureOversampling(BME680_OS_8X);
  bme.setHumidityOversampling(BME680_OS_2X);
  bme.setPressureOversampling(BME680_OS_4X);
  bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
  bme.setGasHeater(320, 150); // 320*C for 150 ms
#endif

#ifdef DEVICE_GKD
  Wire.begin(GKD);
#endif

#ifdef DEVICE_IMP
  Wire.begin(IMP);
#endif

#ifdef DEVICE_MOT
  Wire.begin(MOT);
#endif

#ifdef DEVICE_SEN
  Wire.begin(SEN);
#endif
  
  Wire.onReceive(i2c_rx);
  Wire.onRequest(i2c_req);

  //spi.begin();

  // Assign pins 3, 4, 5 to SERCOM & SERCOM_ALT
  //* Note that since pin 3 and 4 use a SERCOM alt mux, we have to pass in PIO_SERCOM_ALT rather than PIO_SERCOM when we call pinPeripheral()
  /*pinPeripheral(3, PIO_SERCOM_ALT);
  pinPeripheral(4, PIO_SERCOM_ALT);
  pinPeripheral(5, PIO_SERCOM);

  uint8_t i=0;
  spi.beginTransaction(SPISettings(8000000, MSBFIRST, SPI_MODE0));
  spi.transfer(i++);
  spi.endTransaction();*/

  //int sensorValue = analogRead(A0);
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  //float voltage = sensorValue * (5.0 / 1023.0);
  // print out the value you read:
  //Serial.println(voltage);

  // start I2S at 8 kHz with 32-bits per sample
  //I2S.begin(I2S_PHILIPS_MODE, 8000, 32)
  //int sample = I2S.read();

  //if (sample) {
    // if it's non-zero print value to serial
    //Serial.println(sample);
  //}
}

void loop()
{
#ifdef DEVICE_SGP
  sgp.IAQmeasure();
  Serial.print("TVOC (ppb): ");
  Serial.print(sgp.TVOC);
  Serial.println();
  Serial.print("eCO2 (ppm): ");
  Serial.print(sgp.eCO2);
#endif

#ifdef DEVICE_BME
  if (! bme.performReading()) {
    Serial.println("Failed to perform reading :(");
    return;
  }
  Serial.print("Temperature = ");
  Serial.print(bme.temperature);
  Serial.println(" *C");

  Serial.print("Pressure = ");
  Serial.print(bme.pressure / 100.0);
  Serial.println(" hPa");

  Serial.print("Humidity = ");
  Serial.print(bme.humidity);
  Serial.println(" %");

  Serial.print("Gas = ");
  Serial.print(bme.gas_resistance / 1000.0);
  Serial.println(" KOhms");

  Serial.print("Approx. Altitude = ");
  Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
  Serial.println(" m");

  Serial.println();
#endif

  delay(1000);
}

void i2c_rx(int length)
{
  for(int i = 0; i < length; i++)
  {
    rxBuf[i] = Wire.read();
    Serial.print(rxBuf[i]);
  }

  Serial.println();

#ifdef DEVICE_SGP_OLD
  if(rxBuf[0] == 0x36 && rxBuf[1] == 0x82)
  {
    //TODO - Response CMD_GET_SERIAL_ID
    Serial.println("CMD_GET_SERIAL_ID");
  }

  if(rxBuf[0] == 0x20 && rxBuf[1] == 0x08)
  {
    //TODO - Response CMD_MEAS_AIR_QUAL
    Serial.println("CMD_MEAS_AIR_QUAL");
  }
#endif


#ifdef DEVICE_SEN
  memcpy(_pkt._8bit, rxBuf, 2);

  switch(_pkt._16bit[0])
  {
    case AEROBIC_HEART_RATE_LOWER_LIMIT:
      Wire.write(0x01);
      Wire.write(0x02);
      break;

    case AEROBIC_HEART_RATE_UPPER_LIMIT:
      Wire.write(0x03);
      Wire.write(0x04);
      break;

    case AEROBIC_THRESHOLD:
      Wire.write(0x05);
      Wire.write(0x06);
      break;

#ifdef DEVICE_SGP
    case CMD_TVOC:
      _pkt._32bit[0] = sgp.TVOC;
      for(int i = 0; i < 4; i++)
        Wire.write(_pkt._8bit[i]);
      break;

    case CMD_ECO2:
      _pkt._32bit[0] = sgp.eCO2;
      for(int i = 0; i < 4; i++)
        Wire.write(_pkt._8bit[i]);
      break;
#endif

#ifdef DEVICE_BME
    case CMD_TEMPERATURE:
      _pkt._32bit[0] = bme.temperature;
      for(int i = 0; i < 4; i++)
        Wire.write(_pkt._8bit[i]);
      break;

    case CMD_PRESSURE:
      _pkt._32bit[0] = bme.pressure / 100.0;
      for(int i = 0; i < 4; i++)
        Wire.write(_pkt._8bit[i]);
      break;

    case CMD_HUMIDITY:
      _pkt._32bit[0] = bme.humidity;
      for(int i = 0; i < 4; i++)
        Wire.write(_pkt._8bit[i]);
      break;

    case CMD_GAS:
      _pkt._32bit[0] = bme.gas_resistance / 1000.0;
      for(int i = 0; i < 4; i++)
        Wire.write(_pkt._8bit[i]);
      break;

    case CMD_ALTITUDE:
      _pkt._32bit[0] = bme.readAltitude(SEALEVELPRESSURE_HPA);
      for(int i = 0; i < 4; i++)
        Wire.write(_pkt._8bit[i]);
      break;
#endif
  }
#endif

  for(int i = 0; i < length; i++)
  {
    rxBuf[i] = 0;
  }
}

void i2c_req()
{
  //Wire.write(0x66); //0x9966 Working
  //Wire.write(0x99); 
  
#ifdef DEVICE_GKD
  Wire.write(0xce);
  Wire.write(0xfa);
#endif
  
#ifdef DEVICE_MOT
  Wire.write(0x10);
  Wire.write(0x4d);
#endif

#ifdef DEVICE_MOT
  Wire.write(0x10);
  Wire.write(0x4d);
#endif

#ifdef DEVICE_SEN
#ifdef DEVICE_SGP
  Serial.println("Request received");
  length = 2;
  _pkt._16bit[0] = SOF;
  _pkt._16bit[1] = length;
  _pkt._16bit[2] = CMD_TVOC;
  _pkt._16bit[3] = CMD_ECO2;
  _pkt._16bit[4] = EOF;
  Wire.write(_pkt._8bit, 10);
#endif

#ifdef DEVICE_BME
  Serial.println("Request received");
  length = 5;
  _pkt._16bit[0] = SOF;
  _pkt._16bit[1] = length;
  _pkt._16bit[2] = CMD_TEMPERATURE;
  _pkt._16bit[3] = CMD_PRESSURE;
  _pkt._16bit[4] = CMD_HUMIDITY;
  _pkt._16bit[5] = CMD_GAS;
  _pkt._16bit[6] = CMD_ALTITUDE;
  _pkt._16bit[7] = EOF;
  Wire.write(_pkt._8bit, 16);
#endif
#endif
}

void SERCOM4_Handler() {
  myWire.onService();
}
